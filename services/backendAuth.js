import prisma from "../lib/prisma";
import {bcrptHash,hashCheck} from "../lib/helper"
import {generateUserToken} from "../services/JWTservice"
import {setSessionData,getUserByEmail,getUser} from "../services/AuthService"

export const registerUser = async(requestData) => {
    const bymail = await getUserByEmail(requestData)
    const byphone = await getUser(requestData)
    if(bymail) return {status:400,message:"This Email Already Exist!"}
    if(byphone) return {status:400,message:"This Phone Number Already Exist!"}
    const hashPass = await bcrptHash(requestData.password)
    const user = await prisma.user.create({
        data: {
          firstName: data.firstName,
          lastName: data.lastName,
          name: data.lastName,
          email: requestData.email,
          password: hashPass,
          phoneNumber: requestData.phoneNumber
        },
    });
    const token = await generateUserToken({...user,...{platform:"DASHBOARD_USER"}})
    const tokenUser = {user, ...{token:token}};
      await setSessionData(tokenUser)
    return tokenUser;
}
export const loginUser = async(requestData) => {
    const user = await prisma.user.findFirst({
        where: {
          email: requestData.email
        }
    });
    if(!user) return {status:400,message:"This Email is Not Found!"}
    const hashChk = await hashCheck(requestData.password,user.password)
    if(!hashChk) return {status:400,message:"Incorrect Password!"}
    const token = await generateUserToken({...user,...{platform:"DASHBOARD_USER"}})
    const tokenUser = {user, ...{token:token}};
    await setSessionData(user)
    return tokenUser;
}