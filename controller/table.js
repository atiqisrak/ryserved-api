import prisma from "../lib/prisma";

export const addTable = async(data) => {
    try {
        const table = await prisma.Table.create({
            data: {
                subAssetComponent: { connect: { id: data.subAssetCompId } },
                type: data.type,
                capacity: data.capacity,
                position: data.position,
                size: data.size,
                status: data.status ? data.status : true
            }
        });
        return table;
    } catch (error) {
        return error;
    }
}
export const getAllTable = async() => {
    const tables = await prisma.Table.findMany({
        include:{
            subAssetComponent: {
                include: {
                    subAsset: true
                }
            }
        }
    })
    return tables;
}

export const getSingleTable = async(id) => {
    const table = await prisma.Table.findMany({
        where:{
            id:id
        },
        include:{
            subAssetComponent: {
                include: {
                    subAsset: {select: {id:true,propertyName:true}}
                }
            }
        }
    })
    return table;
}
export const updateTable = async(id,data) => {
    try {
        const table = await prisma.Table.update({
            where: {
                id: id
            },
            data:{
                asset: { connect: { id: data.assetId } },
                type: data.type,
                capacity: data.capacity,
                position: data.position,
                size: data.size,
                status: data.status
            },
        });
        return table;
    } catch (error) {
        console.log('Error updating asset:', error);
    }
}
export const destroyTable = async(id) => {
    const table = await prisma.Table.delete({
        where:{
            id: { id }
        }
    });
    return table;
}