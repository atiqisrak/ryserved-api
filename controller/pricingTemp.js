import prisma from "../lib/prisma";

export const createPriceTemp = async(data) => {
    try {
        const pricingTemp = await prisma.PricingTemplate.create({
            data: {
                asset: {connect: {id: data.assetId}},
                tempName: data.tempName,
                type: data.type,
                currency: data.currency?? "BDT",
                basePrice: data.basePrice,
                discount: data.discount,
                pricing: data.price,
                status: data.status ?? true
            },
        });
        return pricingTemp;
    } catch (error) {
        console.log('Error creating pricingTemp:', error);
    }
}

export const getAllPriceTemp = async() => {
    try {
        const pricingTemp = await prisma.PricingTemplate.findMany({
            include: {
                asset:{select :{
                    propertyName:true,
                    country:true,
                    city:true
                }}
            }
        });  
        return pricingTemp; 
    } catch (error) {
        console.log(error);
    }
}
export const updatePriceTemp = async(id,data) => {
    try {
        const pricingTemp = await prisma.PricingTemplate.update({
            where:{
                id: id
            },
            data: {
                tempName: data.tempName,
                type: data.type,
                currency: data.currency?? "BDT",
                basePrice: data.basePrice,
                discount: data.discount,
                pricing: data.price,
                status: data.status
            },
        });
        return pricingTemp;
    } catch (error) {
        console.log(error);
    }
}

export const getPriceTemp = async(id) => {
    const pricingTemp = await prisma.PricingTemplate.findFirst({
        where: {
            id: id,
        },
        // include:{
        //     business: true
        // }
    });
    return pricingTemp;
}

export const destroypriceTemp = async(id) => {
    const pricingTemp = await prisma.PricingTemplate.delete({
        where:{
            id: { id }
        }
    });
    return pricingTemp;
}