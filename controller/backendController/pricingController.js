import prisma from "../../lib/prisma";

export const makePricing = async(data) => {
    try {
        const pricing = await prisma.Pricing.create({
            data: {
                pricingTemplate: { connect: { id: data.priceTempId } },
                subAsset: { connect: { id: data.subAssetId } },
                subAssetComp: { connect: { id: data.subAssetCompId } },
                type: data.type,
                currency: data.currency,
                basePrice: data.basePrice,
                discount: data.discount,
                pricing: data.pricing,
                status: data.status ? data.status : true
            }
        });
        return pricing;
    } catch (error) {
        return error;
    }
}
export const getAllPricing = async(subAssetCompId,reservationCategory) => {
    const pricings = await prisma.Pricing.findMany({
        where: {
            subAssetCompId:subAssetCompId,
            subAssetComponent: {
                reservationCategory: reservationCategory
            }
        },
        include: {
            subAssetComponent: {
                select: {
                    id: true,
                    listingName: true,
                    reservationCategory: true
                },
            },
        },
    })
    return pricings;
}

export const getSinglePricing = async(id) => {
    const pricing = await prisma.Pricing.findMany({
        where:{
            id:id
        },
        include: {
            subAssetComponent: {
                select: {
                    id: true,
                    listingName: true,
                    reservationCategory: true
                },
            },
        },
    })
    return pricing;
}
export const updatePricing = async(id,data) => {
    try {
        const pricing = await prisma.Pricing.update({
            where: {
                id: id
            },
            data:{
                subAssetComponent: { connect: { id: data.subAssetCompId } },
                type: data.type,
                roomNo: data.roomNo,
                breakfast: data.breakfast,
                accomodationCapacity: data.accomodationCapacity,
                extraBedPolicy: data.extraBedPolicy,
                status: data.status
            },
        });
        return pricing;
    } catch (error) {
        console.log('Error updating asset:', error);
    }
}
export const destroyPricing = async(id) => {
    const pricing = await prisma.Pricing.delete({
        where:{
            id: { id }
        }
    });
    return pricing;
}