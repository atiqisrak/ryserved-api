import prisma from "../../lib/prisma";
export const createPayment = async(data) => {
    try {
        const payment = await prisma.Payment.create({
            data: {
                booking: {connect: {id: data.bookingId}},
                paymentDate: data.paymentDate,
                paymentInfo: data.paymentInfo,
                status: data.status
            }
        });
        return payment; 
    } catch (error) {
        console.log('Error creating Payment:', error);
    }
}

export const getAllPayment = async() => {
    const payments = await prisma.Payment.findMany({
        include: {
            booking:  true
        }
    })
    // return business;
    return payments;
}
export const getPayment = async(id) => {
    const payment = await prisma.Payment.findMany({
        where: {
            id:id
        }
    })
    return payment;
}

export const updatePayment = async(id,data) => {
    try {
        const payment = await prisma.Payment.update({
            where:{
                id: id,
            },
            data:{
                booking: {connect: {id: data.bookingId}},
                paymentDate: data.paymentDate,
                paymentInfo: data.paymentInfo,
                status: data.status
            }
        });
        return payment;
    } catch (error) {
        console.log('Error updating post:', error);
    }
}
export const destroyPayment = async(id) => {
    const payment = await prisma.Payment.delete({
        where:{
            id: id
        }
    });
    return payment;
}