import prisma from "../../lib/prisma";

export const addSubAssetComponent = async(data) => {
    try {
        // return data;
        const subassetComp = await prisma.SubAssetComponent.create({
            data: {
                asset: { connect: { id: data.assetId } },
                subAsset: { connect: { id: data.subAssetId } },
                type: data.type,
                listingName: data.listingName,
                slot: data.slot,
                image: data.image,
                description: data.description,
                reservationCategory: data.reservationCategory,
                status: data.status ? data.status : true
            }
        });
        return subassetComp;
    } catch (error) {
        return error;
    }
}
export const getAllSubAssetComponent = async(subAssetId) => {
    const subassetComp = await prisma.SubAssetComponent.findMany({
        where: {
            subAssetId : subAssetId
        },
        include:{
            asset:{select: {id:true,propertyName:true}},
            prices: true
        }
    })
    return subassetComp;
}

export const getSingleSubAssetComponent = async(id) => {
    // return id;
    const subassetComp = await prisma.SubAssetComponent.findMany({
        where:{
            id:id
        },
        include:{
            asset: {select: {id:true,propertyName:true}}
        }
    })
    return subassetComp;
}
export const updateSubAssetComponent = async(id,data) => {
    try {
        const prevasset = await prisma.SubAssetComponent.findFirst({where: {
            id: id
        }})
        const prepareData = {...prevasset,...data};
        let assetConn = data.assetId ? data.assetId : prepareData.assetId
        let subAssetConn = data.subAssetId ? data.subAssetId : prepareData.subAssetId
        prepareData.asset = { connect: { id: assetConn }}
        prepareData.subAsset = { connect: { id: subAssetConn }}
        delete prepareData['id']
        delete prepareData['assetId']
        delete prepareData['subAssetId']
        delete prepareData['createdAt']
        delete prepareData['updatedAt']
        delete prepareData['deleted']

        const subassetComp = await prisma.SubAssetComponent.update({
            where: {
                id: id
            },
            data:prepareData
        });
        return subassetComp;
    } catch (error) {
        console.log('Error updating asset:', error);
    }
}
export const destroySubAssetComponent = async(id) => {
    const subassetComp = await prisma.SubAssetComponent.delete({
        where:{
            id: { id }
        }
    });
    return subassetComp;
}