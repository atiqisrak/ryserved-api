import prisma from "../../lib/prisma";

export const addTable = async (data) => {
  try {
    const table = await prisma.Table.create({
      data: {
        subAssetComponent: { connect: { id: data.subAssetCompId } },
        type: data.type,
        capacity: data.capacity,
        position: data.position,
        size: data.size,
        status: data.status ? data.status : true,
      },
    });
    return table;
  } catch (error) {
    return error;
  }
};
export const getAllTable = async (subAssetCompId, reservationCategory) => {
  try {
    const tables = await prisma.Table.findMany({
        where: {
            subAssetCompId:subAssetCompId,
            subAssetComponent: {
                reservationCategory: reservationCategory
            }
        },
        include: {
            subAssetComponent: {
                select: {
                    id: true,
                    listingName: true,
                    reservationCategory: true
                },
            },
        },
    });
    return tables;
  } catch (error) {
    console.error("Error in getAllTable:", error.message);
    throw new Error("Failed to fetch table data");
    return error;
  }
};

export const getSingleTable = async (id) => {
  const table = await prisma.Table.findMany({
    where: {
      id: id,
    },
    include: {
        subAssetComponent: {
            select: {
                id: true,
                listingName: true,
                reservationCategory: true
            },
        },
    },
  });
  return table;
};
export const updateTable = async (id, data) => {
  try {
    const table = await prisma.Table.update({
      where: {
        id: id,
      },
      data: {
        asset: { connect: { id: data.assetId } },
        type: data.type,
        capacity: data.capacity,
        position: data.position,
        size: data.size,
        status: data.status,
      },
    });
    return table;
  } catch (error) {
    console.log("Error updating asset:", error);
  }
};
export const destroyTable = async (id) => {
  const table = await prisma.Table.delete({
    where: {
      id: { id },
    },
  });
  return table;
};
