import prisma from "../../lib/prisma";

export const addAmenities = async(amenitiesData) => {
    try {
        const ameni = amenitiesData.data.map((amenity) => ({
            businessId: amenitiesData.businessId,
            ...amenity,
          }));
        //   return amenities; 
        const amenities = await prisma.amenities.createMany({
            data: ameni
        });
        return amenities; 
    } catch (error) {
        console.log('Error creating Amenities:', error);
    }
}

export const getAllAmenities = async(businessId) => {
    const amenities = await prisma.amenities.findMany({
        where:{
            businessId:businessId
        },
        // include: {
        //     business: {
        //       select: {
        //         id: true,
        //         businessName: true,
        //         businessType: true
        //       },
        //     },
        // },
    })
    // return business;
    return amenities;
}
export const getSingleAmenities = async(id) => {
    const amenities = await prisma.amenities.findMany({
        where: {
            id:id
        },
        include: {
            business: {
              select: {
                id: true,
                businessName: true,
                businessType: true
              },
            },
          }
    })
    return amenities;
}

export const updateAmenities = async(id,data) => {
    try {
        const prevasset = await prisma.amenities.findFirst({where: {
            id: id
        }})
        const prepareData = {...prevasset,...data};
        let businessConn = data.businessId ? data.businessId : prepareData.businessId
        prepareData.business = { connect: { id: businessConn }}
        delete prepareData['id']
        delete prepareData['businessId']
        delete prepareData['createdAt']
        delete prepareData['updatedAt']
        delete prepareData['deleted']
        const amenities = await prisma.amenities.update({
            where:{
                id: id,
            },
            data:prepareData
        });
        return amenities;
    } catch (error) {
        console.log('Error updating post:', error);
    }
}
export const destroyAmenities = async(id) => {
    const amenities = await prisma.amenities.delete({
        where:{
            id: id
        }
    });
    return amenities;
}