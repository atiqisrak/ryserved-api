import { slugify } from "../../lib/helper";
import prisma from "../../lib/prisma";
import BusinessResources from "../../resources/BusinessResources";

export const createBusiness = async(data) => {
    try {
        const business = await prisma.business.create({
            data: {
                businessName: data.business_name,
                slug: slugify(data.business_name),
                shortDescription: data.short_description,
                longDescription: data.long_description,
                businessType: data.business_type,
                serviceType: data.service_type,
                businessCategory: data.business_category,
                country: data.country,
                city: data.city,
                locationPoint: data.location_point,
                businessOwner: data.business_owner,
                businessManager: data.business_manager,
                numberOfEmployee: data.number_of_employee,
                tradeLicence: data.trade_licence,
                tin: data.tin,
                bin: data.bin,
                user:{
                    connect:{
                        id: data.userId
                    }
                },
                status: data.status
            }
        });
        return business; 
    } catch (error) {
        console.log('Error creating Business:', error);
        return error
    }
}

export const getAllBusiness = async(userid) => {
    
    const business = await prisma.business.findMany({
        where:{
            userId: userid
        },
        include: {
            user: {
              select: {
                id: true,
                name: true,
                email: true
              },
            },
          },
    })
    // return business;
    return (new BusinessResources).handleCollection(business);
}
export const getBusiness = async(id) => {
    const business = await prisma.business.findMany({
        where: {
            id:id
        }
    })
    return (new BusinessResources).handleCollection(business);
}

export const updateBusiness = async(id,data) => {
    try {
        const business = await prisma.business.update({
            where:{
                id: id,
            },
            data:{
                businessName: data.business_name,
                slug: slugify(data.business_name),
                shortDescription: data.short_description,
                longDescription: data.long_description,
                businessType: data.business_type,
                serviceType: data.service_type,
                businessCategory: data.business_category,
                country: data.country,
                city: data.city,
                locationPoint: data.location_point,
                businessOwner: data.business_owner,
                businessManager: data.business_manager,
                numberOfEmployee: data.number_of_employee,
                tradeLicence: data.trade_licence,
                tin: data.tin,
                bin: data.bin,
                user:{
                    connect:{
                        id: data.userId
                    }
                },
                status: data.status
            }
        });
        return (new BusinessResources).handleCollection(business);
    } catch (error) {
        console.log('Error updating post:', error);
    }
}
export const destroyBusiness = async(id) => {
    const business = await prisma.business.delete({
        where:{
            id: id
        }
    });
    return (new BusinessResources).handleSingleObject(business);
}