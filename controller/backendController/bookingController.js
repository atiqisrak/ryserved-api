import prisma from "../../lib/prisma";

export const createBooking = async(data) => {
    try {
        const bookingData = {
            subAsset: {connect: {id: data.subAssetId}},
            subAssetComponent: {connect: {id: data.subAssetCompId}},
            owner: {connect: {id: data.ownerId}},
            customer: {connect: {id: data.customerId}},
            customerName: data.customerName,
            phoneNumber: data.phoneNumber,
            startDate: new Date(data.startDate),
            endDate: new Date(data.endDate),
            amount: data.amount,
            vat: data.vat,
            discount: data.discount,
            grandTotal: data.grandTotal,
            status: data.status ?? "ACTIVE"
        };
        if (data.tableId !== '') {
            bookingData.table = {connect: {id: data.tableId}};
        }
        if (data.seatBedId !== '') {
            bookingData.seatBed = {connect: {id: data.seatBedId}};
        }
        const booking = await prisma.Booking.create({
            data: bookingData
        });
        return booking; 
    } catch (error) {
        console.log('Error creating Booking:', error);
    }
}

export const getAllBooking = async() => {
    const bookings = await prisma.Booking.findMany({
        include: {
            owner:  {
                select: {
                    id:true,name:true,phoneNumber:true
                }
            },
            customer:  {
                select: {
                    id:true,name:true,phoneNumber:true
                }
            },
            subAssetComponent: {
                select: {
                    id:true,listingName:true,type:true,reservationCategory:true
                }
            },
            table: {
                select: {
                    id:true,capacity:true,type:true,size:true
                }
            },
            seatBed: {
                select: {
                    id:true,type:true,roomNo:true
                }
            }
        }
    })
    // return business;
    return bookings;
}
export const getBooking = async(id) => {
    const booking = await prisma.Booking.findMany({
        where: {
            id:id
        }
    })
    return booking;
}

export const updateBooking = async(id,data) => {
    try {
        const booking = await prisma.Booking.update({
            where:{
                id: id,
            },
            data:{
                subAsset: {connect: {id: data.subAssetId}},
                subAssetComponent: {connect: {id: data.subAssetCompId}},
                table: {connect: {id: data.tableId}},
                seatBed: {connect: {id: data.seatBedId}},
                owner: {connect: {id: data.ownerId}},
                customer: {connect: {id: data.customerId}},
                customerName: data.customerName,
                phoneNumber: data.phoneNumber,
                startDate: data.startDate,
                endDate: data.endDate,
                amount: data.amount,
                vat: data.vat,
                discount: data.discount,
                grandTotal: data.grandTotal,
                status: data.status
            }
        });
        return booking;
    } catch (error) {
        console.log('Error updating post:', error);
    }
}
export const destroyBooking = async(id) => {
    const booking = await prisma.Booking.delete({
        where:{
            id: id
        }
    });
    return booking;
}