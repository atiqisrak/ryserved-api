import prisma from "../lib/prisma";

export const addSubAssetComponent = async(data) => {
    try {
        // return data;
        const subassetComp = await prisma.SubAssetComponent.create({
            data: {
                asset: { connect: { id: data.assetId } },
                subAsset: { connect: { id: data.subAssetId } },
                type: data.type,
                listingName: data.listingName,
                description: data.description,
                reservationCategory: data.reservationCategory,
                status: data.status ? data.status : true
            }
        });
        // const subassetComp = await prisma.Pricing.create({
        //     data: {
        //         pricingTemplate: { connect: { id: data.priceTempId } },
        //         subAssetComp: { connect: { id: data.subAssetCompId } },
        //         type: data.type,
        //         currency: data.currency?? "BDT",
        //         basePrice: data.basePrice,
        //         discount: data.discount,
        //         pricing: data.price,
        //         status: data.status ?? 1
        //     }
        // });
        return subassetComp;
    } catch (error) {
        return error;
    }
}
export const getAllSubAssetComponent = async(queData) => {
    const {from,to,populerItem,type} = queData
    let orderBy = {};
    if (populerItem=="yes") {
        orderBy = {
            asset: {
                bookingCount: 'desc'
            }
        };
    }
    const subassetComp = await prisma.SubAssetComponent.findMany({
        skip: Number(from),
        take: Number(to),
        where: {
            type: type
        },
        orderBy: orderBy,
        include:{
            asset: {select: {id:true,propertyName:true,city:true,country:true,bookingCount:true}}
        },
    });
    return subassetComp;
}

export const getSingleSubAssetComponent = async(id) => {
    const subassetComp = await prisma.SubAssetComponent.findMany({
        where:{
            id:id
        },
        include:{
            asset: {
                // include:{
                //     business: {
                //         select:{id:true,userId:true}
                //     }
                // },
                select: {id:true,propertyName:true,businessId:true}
            },
            tables: {select: {id:true,type:true,capacity:true,position:true,size:true}}
        }
    })
    return subassetComp;
}
export const updateSubAssetComponent = async(id,data) => {
    try {
        const subassetComp = await prisma.SubAssetComponent.update({
            where: {
                id: id
            },
            data:{
                asset: { connect: { id: data.assetId } },
                subAsset: { connect: { id: data.subAssetId } },
                type: data.type,
                listingName: data.listingName,
                description: data.description,
                reservationCategory: data.reservationCategory,
                status: data.status
            },
        });
        return subassetComp;
    } catch (error) {
        console.log('Error updating asset:', error);
    }
}
export const destroySubAssetComponent = async(id) => {
    const subassetComp = await prisma.SubAssetComponent.delete({
        where:{
            id: { id }
        }
    });
    return subassetComp;
}