import prisma from "../lib/prisma";
const { slugify } = require("../lib/helper");

export const addSubAsset = async(data) => {
    try {
        // return data;
        const subasset = await prisma.SubAsset.create({
            data: {
                asset: { connect: { id: data.assetId } },
                price: { connect: { id: data.priceId } },
                amenities: { connect: { id: data.amenitiesId } },
                floor: data.floor,
                sqft: data.sqft,
                status: data.status ? data.status : true
            }
        });
        return subasset;
    } catch (error) {
        return error;
    }
}
export const getAllSubAsset = async() => {
    const subassets = await prisma.SubAsset.findMany({
        include:{
            asset: {
                include: {
                    business: {
                        include: {
                            user:{select: {id:true,name:true}}
                        }  
                    }
                }
            }
        }
    })
    return subassets;
}

export const getSingleSubAsset = async(id) => {
    const subassets = await prisma.SubAsset.findMany({
        where:{
            id:id
        },
        include:{
            asset: {
                include: {
                    user: {select: {id:true,name:true}}
                }
            }
        }
    })
    return subassets;
}
export const updateSubAsset = async(id,data) => {
    try {
        const subasset = await prisma.SubAsset.update({
            where: {
                id: id
            },
            data:{
                assetType: data.asset_type,
                propertyName: data.property_name,
                slug: slugify(data.property_name),
                business: {
                    connect: {
                        id: data.businessId
                    }
                },
                country: data.country,
                city: data.city,
                locationPoint: data.location_point,
                geoTag: data.geo_tag,
                noOfRoom: data.no_of_room,
                status: data.status
            },
        });
        return subasset;
    } catch (error) {
        console.log('Error updating asset:', error);
    }
}
export const destroySubAsset = async(id) => {
    const subasset = await prisma.SubAsset.delete({
        where:{
            id: { id }
        }
    });
    return subasset;
}