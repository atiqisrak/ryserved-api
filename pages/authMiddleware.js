import {userByToken} from "../services/JWTservice"

const authenticate = (handler) => async (req, res) => {
  try {
    // Get the token from the 'Authorization' header
    const token = req.headers.authorization;
    // Verify the token
    const authUser = await userByToken(token)
    // Attach the user information to the request for further processing
    req.user = authUser;
    // Proceed to the actual API route
    return handler(req, res);
  } catch (error) {
    return res.status(401).json({ error: 'Unauthorized' });
  }
};

export default authenticate;