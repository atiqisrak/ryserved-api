import { addSubAsset,getAllSubAsset,getSingleSubAsset,updateSubAsset,destroySubAsset } from "../../../controller/subasset";

export default async function handler(req,res){
    try {
        switch (req.method) {
            case "POST": {
                const subasset = await addSubAsset(req.body)
                return res.status(201).json(subasset)
            }
            case "GET": {
                const {id} = req.query
                let subasset = {};
                  if(!id){
                    subasset = await getAllSubAsset();
                  }else{
                    subasset = await getSingleSubAsset(id);
                  }
                return res.status(200).json(subasset)
            }
            case 'DELETE': {
                const {id} = req.query
                const destroy = await destroySubAsset(id);
                return res.status(200).json({message:'Sub Asset Deleted Successful',subasset: destroy});
              }

            case 'PUT': {
                const {id} = req.query
                const subasset = await updateSubAsset(id,req.body);
                return res.status(200).json({message:'Sub Asset Updated Successful',subasset: subasset});
            }
            default:{
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        return res.status(500).json({ message: error });
    }
}