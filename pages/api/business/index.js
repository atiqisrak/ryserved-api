import { createBusiness, destroyBusiness, getAllBusiness, getBusiness, updateBusiness } from "../../../controller/business"

export default async function handler(req,res){
    try {
        switch(req.method){
            case "POST": {
                const business = await createBusiness(req.body)
                return res.status(201).json(business);
            }
            case "GET": {
                const {id} = req.query
                if(!id){
                    const business = await getAllBusiness();
                    return res.status(201).json(business);
                }
                const business = await getBusiness(id);
                return res.status(200).json(business);
            }
            case "PUT": {
                const {id} = req.query
                const business = await updateBusiness(id,req.body);
                return res.status(200).json(business);
            }
            case "DELETE": {
                const {id} = req.query
                const business = await destroyBusiness(id);
                return res.status(200).json({ message: 'Business Delete successfully',item:business });
            }
            default: {
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        console.error('Error:', error);
        return res.status(500).json({ message: 'Internal Server Error' });
    }
}