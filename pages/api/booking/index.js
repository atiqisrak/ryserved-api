import { createBooking, destroyBooking, getAllBooking, getBooking, updateBooking } from "../../../controller/booking"
import authenticate from "../../authMiddleware";

const handler = async(req,res) => {
    try {
        switch(req.method){
            case "POST": {
                const booking = await createBooking(req)
                return res.status(201).json(booking);
            }
            case "GET": {
                const {id} = req.query
                if(!id){
                    const booking = await getAllBooking();
                    return res.status(201).json(booking);
                }
                const booking = await getBooking(id);
                return res.status(200).json(booking);
            }
            case "PUT": {
                const {id} = req.query
                const booking = await updateBooking(id,req.body);
                return res.status(200).json(booking);
            }
            case "DELETE": {
                const {id} = req.query
                const booking = await destroyBooking(id);
                return res.status(200).json({ message: 'booking Delete successfully',item:booking });
            }
            default: {
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        console.error('Error:', error);
        return res.status(500).json({ message: 'Internal Server Error' });
    }
}
export default authenticate(handler)