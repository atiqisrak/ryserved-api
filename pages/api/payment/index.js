import { createPayment, destroyPayment, getAllPayment, getPayment, updatePayment } from "../../../controller/payment"

export default async function handler(req,res){
    try {
        switch(req.method){
            case "POST": {
                const payment = await createPayment(req.body)
                return res.status(201).json(payment);
            }
            case "GET": {
                const {id} = req.query
                if(!id){
                    const payment = await getAllPayment();
                    return res.status(201).json(payment);
                }
                const payment = await getPayment(id);
                return res.status(200).json(payment);
            }
            case "PUT": {
                const {id} = req.query
                const payment = await updatePayment(id,req.body);
                return res.status(200).json(payment);
            }
            case "DELETE": {
                const {id} = req.query
                const payment = await destroyPayment(id);
                return res.status(200).json({ message: 'Payment Delete successfully',item:payment });
            }
            default: {
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        console.error('Error:', error);
        return res.status(500).json({ message: 'Internal Server Error' });
    }
}