import { addSubAssetComponent,getAllSubAssetComponent,getSingleSubAssetComponent,updateSubAssetComponent,destroySubAssetComponent } from "../../../controller/subassetComponent";

export default async function handler(req,res){
    try {
        switch (req.method) {
            case "POST": {
                const subassetComp = await addSubAssetComponent(req.body)
                return res.status(201).json(subassetComp)
            }
            case "GET": {
                const {id} = req.query
                let subassetComp = {};
                  if(!id){
                    subassetComp = await getAllSubAssetComponent(req.query);
                  }else{
                    subassetComp = await getSingleSubAssetComponent(id);
                  }
                return res.status(200).json(subassetComp);
            }
            case 'DELETE': {
                const {id} = req.query
                const destroy = await destroySubAssetComponent(id);
                return res.status(200).json({message:'Sub Asset Component Deleted Successful',subassetComp: destroy});
              }

            case 'PUT': {
                const {id} = req.query
                const subassetComp = await updateSubAssetComponent(id,req.body);
                return res.status(200).json({message:'Sub Asset Component Updated Successful',subasset: subassetComp});
            }
            default:{
                return res.status(405).json({ message: 'Method Not Allowed' });
            }
        }
    } catch (error) {
        return res.status(500).json({ message: error });
    }
}