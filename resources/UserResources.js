// const UserResources = async (data) => {
//     return data.map((element) => {
//       return {
//         id: element.id,
//         customer_name: element.name,
//         customer_email: element.email,
//         dob: element.birthDate,
//       };
//     });
//   };

  class UserResources {
      async handleCollection(collection) {
        // if(collection.length == 1) handleSingleObject([...collection])
        return collection.map((element) => {
          return {
            id: element.id,
            customer_name: element.name,
            customer_email: element.email,
            dob: element.birthDate,
            user_type: element.userType,
            phone_number: element.phoneNumber,
            country: element.country,
            city: element.city,
            gl_location_link: element.location,
            residence_address: element.residenceAddress,
            occupation: element.occupation,
            designation: element.designation,
            nid: element.nid,
            tin: element.tin,
            having_business: element.havingBusiness,
            business: element.business,
            // password: element.password,
            created_at: element.createdAt,
            updated_at: element.updatedAt,
            status: element.status == true ? 'Active' : 'Deactive'
          };
        });
      }
    
      async handleSingleObject(obj) {
          return {
            id: obj.id,
            customer_name: obj.name,
            customer_email: obj.email,
            dob: obj.birthDate,
            user_type: obj.userType,
            phone_number: obj.phoneNumber,
            country: obj.country,
            city: obj.city,
            gl_location_link: obj.location,
            residence_address: obj.residenceAddress,
            occupation: obj.occupation,
            designation: obj.designation,
            nid: obj.nid,
            tin: obj.tin,
            having_business: obj.havingBusiness,
            // password: obj.password,
            created_at: obj.createdAt,
            updated_at: obj.updatedAt,
            status: obj.status == true ? 'Active' : 'Deactive'
        };
    }
}
export default UserResources;